package zadatak.htec.example.com.htectestsample.activities;

import android.os.Bundle;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import zadatak.htec.example.com.htectestsample.R;
import zadatak.htec.example.com.htectestsample.adapters.RecyclerAdapter;
import zadatak.htec.example.com.htectestsample.controller.Controllers;
import zadatak.htec.example.com.htectestsample.model.DataModel;

public class MainActivity extends AppCompatActivity {

    Retrofit retrofit;
    Controllers userAPI;
    RecyclerAdapter mAdapter;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    ArrayList<DataModel> centersList;

    CountingIdlingResource espressoTestIdlingResource = new CountingIdlingResource("Network_Call");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(Controllers.JSON_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        userAPI = retrofit.create(Controllers.class);

        centersList= new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapter(getBaseContext(), centersList, MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);


        getJsonData();

        }


    // Make server call
    private void getJsonData(){

        Call<List<DataModel>> getData = userAPI.getJsonDAta();
        espressoTestIdlingResource.increment();

        getData.enqueue(new Callback<List<DataModel>>() {
            @Override
            public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {
                List<DataModel> obj = response.body();
                Log.e("CODE", String.valueOf(response.code()));
                forEachLoop(obj);
                espressoTestIdlingResource.decrement();

            }

            @Override
            public void onFailure(Call<List<DataModel>> call, Throwable t) {
                Log.e("FAILURE ", t.getMessage());
                espressoTestIdlingResource.decrement();
            }
        });

    }

    // Fetch JSON Data
    private void forEachLoop(List<DataModel> arr){
        for(DataModel obj : arr){
            obj.setType(0);
            obj.setImage(obj.getImage().replace("http", "https"));
            centersList.add(obj);
        }
        mAdapter.notifyDataSetChanged();
    }

    public CountingIdlingResource getEspressoIdlingResourceForMainActivity() {
        return espressoTestIdlingResource;
    }


}
