package zadatak.htec.example.com.htectestsample.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import zadatak.htec.example.com.htectestsample.R;

/**
 * Created by Jovan Milutinović on 13.12.2017..
 */
public class SubActivity extends AppCompatActivity {

    ImageView imageFrame;
    TextView titleText;
    TextView descriptionText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subactivity);

        imageFrame = (ImageView)findViewById(R.id.image);
        titleText = (TextView)findViewById(R.id.title);
        descriptionText = (TextView)findViewById(R.id.description);

        Intent i = getIntent();
        setImage(i);
        setText(i);
        setDescription(i);

    }


    private void setImage(Intent i){
        boolean in = i.hasExtra("image");
        if(in == true){
            final String picture = i.getStringExtra("image");

            Picasso.with(this).load(picture).into(imageFrame, new Callback() {
                @Override
                public void onSuccess() {
                    Picasso.with(getBaseContext()).load(picture).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                            final android.view.ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)(imageFrame).getLayoutParams();

                            //Calculate image ratio height/width
                            float ratio = (float) bitmap.getHeight() / (float) bitmap.getWidth();

                            //Get device screen width
                            int screenWidth = getScreenWidth();

                            // Checking if image width is larger than screen width in pixels and make decission
                            // to fill full width and set height or to maintain original width and set heigh
                            if(screenWidth > bitmap.getWidth()){
                                layoutParams.width = (int) bitmap.getWidth();
                                layoutParams.height = (int) ((int) bitmap.getWidth()*ratio);
                            }else{
                                layoutParams.width = (int) screenWidth;
                                layoutParams.height = (int) ((int) screenWidth*ratio);
                            }

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

                }

                @Override
                public void onError() {

                }
            });
        }
    }

    private void setText(Intent i){
        boolean in = i.hasExtra("title");
        if(in == true){
            String title = i.getStringExtra("title");
            titleText.setText(title);
        }
    }

    private void setDescription(Intent i){
        boolean in = i.hasExtra("description");
        if(in == true){
            String description = i.getStringExtra("description");
            descriptionText.setText(description);
        }
    }

    private int getScreenWidth(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }



}
