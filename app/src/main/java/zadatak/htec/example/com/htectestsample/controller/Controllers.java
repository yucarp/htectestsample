package zadatak.htec.example.com.htectestsample.controller;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import zadatak.htec.example.com.htectestsample.model.DataModel;


public interface Controllers {

//     DEVELOPMENT
    String JSON_URL = "https://raw.githubusercontent.com/";


    // GET DATA FROM JSON
    @Headers("Content-type: application/json")
    @GET("danieloskarsson/mobile-coding-exercise/master/items.json")
    Call<List<DataModel>> getJsonDAta();

}
