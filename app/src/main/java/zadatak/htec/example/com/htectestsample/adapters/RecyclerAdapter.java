package zadatak.htec.example.com.htectestsample.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import zadatak.htec.example.com.htectestsample.R;
import zadatak.htec.example.com.htectestsample.activities.MainActivity;
import zadatak.htec.example.com.htectestsample.activities.SubActivity;
import zadatak.htec.example.com.htectestsample.interfaces.IntentMethods;
import zadatak.htec.example.com.htectestsample.model.DataModel;

/**
 * Created by Jovan Milutinović on 13.12.2017..
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IntentMethods {

    List<DataModel> list;
    Context context;
    MainActivity act;


    public RecyclerAdapter(Context context, List<DataModel> list, MainActivity act) {
        this.context = context;
        this.act = act;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        View view;

        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_preofessional_updates, parent, false);
                ViewHolder t = new ViewHolder(view);
                return t;
        }
        return null;

    }



    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        final DataModel object = list.get(position);

        if (object != null) {
            switch (object.getType()) {
                case 0:

                    if(!(object.getImage().equals(""))) {
                        Picasso.with(context)
                                .load(object.getImage())
                                .fit()
                                .centerCrop()
                                .into(((ViewHolder) viewHolder).image);
                    }else{
                        Picasso.with(context)
                                .load(R.drawable.placeholder)
                                .fit()
                                .centerCrop()
                                .into(((ViewHolder) viewHolder).image);
                    }

                    String descCheck= (object.getDescription().length() <= 40) ? object.getDescription() : object.getDescription().substring(0, 40) + "...";
                    ((ViewHolder) viewHolder).description.setText(descCheck);
                    ((ViewHolder) viewHolder).title.setText(object.getTitle());


                    ((ViewHolder) viewHolder).wholeSpace.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            act.sendDataSubActivity(object.getImage(), object.getTitle(), object.getDescription());
                            sendIntent(object.getImage(), object.getTitle(), object.getDescription());
                        }
                    });
                    break;

            }
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }



    public DataModel getItem(int position) {
        return list.get(position);
    }


    @Override
    public int getItemViewType(int position) {
        if (list != null) {
            DataModel object = list.get(position);
            if (object != null) {
                return object.getType();
            }
        }
        return 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout wholeSpace;
        RoundedImageView image;
        TextView title;
        TextView description;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (RoundedImageView) itemView.findViewById(R.id.imageLeft);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            wholeSpace = (RelativeLayout) itemView.findViewById(R.id.firstLay);

        }

    }


    @Override
    public void sendIntent(String image, String title, String description) {
        Intent i = new Intent(context, SubActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("image", image);
        i.putExtra("title", title);
        i.putExtra("description", description);
        context.startActivity(i);
    }


}
