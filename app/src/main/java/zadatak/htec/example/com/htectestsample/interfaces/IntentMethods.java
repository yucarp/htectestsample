package zadatak.htec.example.com.htectestsample.interfaces;

/**
 * Created by Jovan Milutinović on 13.12.2017..
 */
public interface IntentMethods {
    void sendIntent(String image, String title, String description);
}
