package zadatak.htec.example.com.htectestsample.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jovan Milutinović on 13.12.2017..
 */
public class DataModel {

    @SerializedName("image")
    private String image;

    @SerializedName("description")
    private String description;

    @SerializedName("title")
    private String title;

    private int type;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
