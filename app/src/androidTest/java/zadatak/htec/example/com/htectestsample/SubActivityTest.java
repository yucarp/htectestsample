package zadatak.htec.example.com.htectestsample;

import android.support.test.filters.SmallTest;
import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ImageView;
import android.widget.TextView;

import zadatak.htec.example.com.htectestsample.activities.MainActivity;
import zadatak.htec.example.com.htectestsample.activities.SubActivity;

/**
 * Created by Jovan Milutinović on 13.12.2017..
 */
public class SubActivityTest extends ActivityInstrumentationTestCase2<SubActivity> {

    public SubActivityTest() {
        super(SubActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @SmallTest
    public void testImageView(){
        ImageView im = (ImageView)getActivity().findViewById(R.id.imageLeft);
        assertNull(im);
    }

    @SmallTest
    public void testTitle(){
        TextView tit = (TextView)getActivity().findViewById(R.id.title);
        assertNotNull(tit);
    }

    @SmallTest
    public void testDescription(){
        TextView desc = (TextView)getActivity().findViewById(R.id.description);
        assertNotNull(desc);
    }


    @SmallTest
    public void testTitleText(){
        TextView tit = (TextView)getActivity().findViewById(R.id.title);
        String title = tit.getText().toString();
        assertEquals(title, "");
    }

    @SmallTest
    public void testDescriptionText(){
        TextView desc = (TextView)getActivity().findViewById(R.id.description);
        String description = desc.getText().toString();
        assertEquals(description, "");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
