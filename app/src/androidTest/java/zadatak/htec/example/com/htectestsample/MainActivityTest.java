package zadatak.htec.example.com.htectestsample;

import android.support.test.filters.SmallTest;
import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import zadatak.htec.example.com.htectestsample.activities.MainActivity;

/**
 * Created by Jovan Milutinović on 13.12.2017..
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @SmallTest
    public void testRecyclerView(){
        RecyclerView recycler = (RecyclerView)getActivity().findViewById(R.id.recyclerView);
        assertNotNull(recycler);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
