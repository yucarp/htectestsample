package zadatak.htec.example.com.htectestsample;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import zadatak.htec.example.com.htectestsample.activities.MainActivity;
import zadatak.htec.example.com.htectestsample.activities.SubActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Jovan Milutinović on 12.12.2017..
 */
@RunWith(AndroidJUnit4.class)
public class EspressoSimpleTests {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule(MainActivity.class);

    @Rule
    public ActivityTestRule<SubActivity> mActivityRuleSub = new ActivityTestRule(SubActivity.class);


    // Some basic UI tests
    @Test
    public void basicUiTests() {
        CountingIdlingResource mainActivityIdlingResource = mActivityRule.getActivity().getEspressoIdlingResourceForMainActivity();
        // registering MainActivity's idling resource for enabling Espresso sync with MainActivity's background threads
        Espresso.registerIdlingResources(mainActivityIdlingResource);

        // Test scroll to position
        onView(withId(R.id.recyclerView))
                .perform(RecyclerViewActions.scrollToPosition(2));

        // test action click on position
        onView(withId(R.id.recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));

    }


    // Some basic testing intents
    @Test
    public void basicIntentTests() {
        Intents.init();

        mActivityRule.launchActivity(new Intent());
        intended(hasComponent(MainActivity.class.getName()));

        mActivityRuleSub.launchActivity(new Intent());
        intended(hasComponent(SubActivity.class.getName()));

        Intents.release();
    }




}
